#tag Window
Begin Window MainWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   534
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   -1109106120
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Extraction de données"
   Visible         =   True
   Width           =   582
   Begin PushButton MainWindowPushButton
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Création de la feuille Excel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   36
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
   Begin TextArea MainWindowEditField
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   451
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   63
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   542
   End
   Begin Label MainWindowStaticText
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   39
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   193
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Création de la feuille Excel contenant la liste des projets à analyser pour la compagnie sélectionnée."
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   12
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   369
   End
   Begin PopupMenu CompagniePopupMenu
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "hlther\r\ntecheol"
      Italic          =   False
      Left            =   20
      ListIndex       =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      Visible         =   True
      Width           =   161
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  determineEnvironnement
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Function addTitleCell(titre_param As String) As XLSCell
		  Dim c as New XLSCell(titre_param)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 14
		  c.FillColor = &cFFBB44
		  c.FontColor = &cFF0000
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub createChiffrierCompagnie(compagnie_param As String)
		  dim w as XLSWorkBook
		  
		  // create a new workbook
		  w = new XLSWorkbook
		  MainWindowEditField.text = ""
		  
		  w.WindowHeightInTwips = 288
		  w.WindowWidthInTwips = 288
		  
		  // add worksheet to the work book
		  w.workSheets.append CreateChiffrierProjets(compagnie_param)
		  
		  Dim d as New Date
		  //<Data ss:Type="DateTime">2004-12-03T12:00:00.000</Data>
		  Dim dateToday As String = format(d.Year,"0000") + format(d.month,"00") + format(d.day,"00")
		  dim f as FolderItem 
		  f = GetSaveFolderItem("", "Excel XML Workbook " + compagnie_param + " " + dateToday + ".xml")
		  
		  if not(f  is nil) Then
		    // this will OVERWRITE an existing file if it can
		    // so you should do all the normal check if the file exists, do you want to overwrite kind of checks 
		    // before you call this
		    w.save(f)
		    
		    //f.Launch
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createChiffrierProjets(compagnie_param As String) As XLSWorkSheet
		  Dim message,strSQL As String
		  message = ouvertureBD(compagnie_param)
		  
		  Dim ws as XLSWorkSheet
		  
		  // create a new worksheet
		  ws = new XLSWorkSheet
		  
		  // Create title row
		  // add the row to the spreadsheet 
		  ws.AppendRow createTitle
		  
		  // Lecture du réalisé pour les dépenses par code de Grand Livre
		  strSQL = _
		  "SELECT projet_no,projet_titre, projet_titre, parametre_desc_fr AS projet_type_desc, projet_charge_projet_nom, projet_statut " + _
		  "  FROM projet" + _
		  "      INNER JOIN parametre ON  parametre.parametre_nom = 'projettype' " + _
		  "                                          AND  parametre.parametre_cle    = projet.projet_type " + _
		  "  WHERE projet.projet_statut IN('Travaux en cours', 'En suspens', 'Travaux complétés') " + _ 
		  "  ORDER BY  projet_no "
		  
		  Dim projetRS As RecordSet = bdGop.SQLSelect(strSQL)
		  If projetRS = Nil Then Exit
		  Dim index As Integer = 1
		  While Not  projetRS.EOF
		    Dim r as XLSRow =  new XLSRow
		    Dim c as XLSCell
		    index = index + 1
		    
		    c = createStdText(compagnie_param)
		    r.AppendCell c
		    c =createStdText(projetRS.Field("projet_statut").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_type_desc").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_no").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_titre").StringValue)
		    r.AppendCell c
		    c = createStdText(projetRS.Field("projet_charge_projet_nom").StringValue)
		    r.AppendCell c
		    
		    // Lecture de toutes les données estimation et réelle
		    Dim estimeDict As Dictionary = estimeReelExtr(projetRS.Field("projet_no").StringValue)
		    
		    // Revenu estimé
		    Dim MontantDouble1 As Double = estimeDict.Lookup("EstimeMontantFacture", 0)
		    Dim MontantDouble2 As Double = estimeDict.Lookup("EstimeMontantFactureExtra", 0)
		    If MontantDouble1 <> 0 Then
		      c = createStdNumber(MontantDouble1)
		    Else
		      c =  createStdNumber(MontantDouble2)
		    End If
		    r.AppendCell c
		    
		    // Dépenses estimées
		    MontantDouble1 = estimeDict.Lookup("EstimeMontantMO",0) + estimeDict.Lookup("EstimeMontantMAT",0)
		    c = createStdNumber(MontantDouble1)
		    r.AppendCell c
		    
		    // Formule de Marge brute estimée
		    //Dim formule As String = "=SI(G"+ str(index) + "=0; 0; (G"+ str(index) + "-H" + str(index) + ")/G"+ str(index) + ")"
		    Dim formule As String = "=(RC[-2]-RC[-1])/RC[-2]"
		    //Dim formule As String = "=SI(RC[-2]=0;0;(RC[-2]-RC[-1])/RC[-2])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    //Montant total facturé Chef De Projet
		    //r.AppendCell createStdNumber(0)
		    //Dépense estimées Chef De Projet
		    //r.AppendCell createStdNumber(0)
		    
		    // Dépenses CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalMO",0) + estimeDict.Lookup("ReelMontantTotalMAT",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Perdiem en cours
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalPerDiem",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Revenus totaux facturés CMEQ
		    MontantDouble1 = estimeDict.Lookup("ReelMontantTotalFacture",0)
		    c = createStdNumber( MontantDouble1)
		    r.AppendCell c
		    
		    // Marge brute calculée
		    formule = "=RC[-1]-(RC[-3]+RC[-2])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    // Profit net calculé
		    formule = "=RC[-1]-(0.3*RC[-4])"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    // Marge brute %
		    formule = "=RC[-2]/RC[-5]"
		    c = createStdFormula(formule)
		    r.AppendCell c
		    
		    Suivant:
		    MainWindowEditField.text = MainWindowEditField.text + "Projet " + projetRS.Field("projet_no").StringValue + " Traité" + chr(13)
		    ws.AppendRow r
		    projetRS.MoveNext
		  Wend
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  bdGOP.Close
		  bdGop = Nil
		  
		  return ws
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function createStdFormula(formula_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = "0"
		  c.type = XLSCell.typeNumber
		  c.formula = formula_param
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function createStdNumber(nombreDouble_param As Double) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = str(nombreDouble_param)
		  c.type = XLSCell.typeNumber
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function createStdText(text_param As String) As XLSCell
		  Dim c As XLSCell = New XLSCell
		  // cells can have styles (fill color, fill style, text color, fomulas, vlalues, etc)
		  c.FillPattern = XLSStyle.None
		  c.Font = "Calibri"
		  c.FontSize = 11
		  c.value = text_param
		  c.type = XLSCell.typeString
		  
		  Return c
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function createTitle() As XLSRow
		  Dim r as XLSRow =  new XLSRow
		  Dim c as XLSCell
		  
		  c = addTitleCell("Compagnie")
		  r.AppendCell c
		  c = addTitleCell("Statut")
		  r.AppendCell c
		  c = addTitleCell("Type de projet")
		  r.AppendCell c
		  c = addTitleCell("#Projet")
		  r.AppendCell c
		  c = addTitleCell("Description")
		  r.AppendCell c
		  c = addTitleCell("Nom du chargé de projet")
		  r.AppendCell c
		  c = addTitleCell("Revenu estimé")
		  r.AppendCell c
		  c = addTitleCell("Dépenses estimées")
		  r.AppendCell c
		  c = addTitleCell("Marge brute estimée")
		  r.AppendCell c
		  c = addTitleCell("Dépenses")
		  r.AppendCell c
		  c = addTitleCell("Perdiem")
		  r.AppendCell c
		  c = addTitleCell("Revenu facturés")
		  r.AppendCell c
		  c = addTitleCell("Marge brute calculée")
		  r.AppendCell c
		  c = addTitleCell("Profit net")
		  r.AppendCell c
		  c = addTitleCell("Marge brute %")
		  r.AppendCell c
		  
		  MainWindowEditField.text = "Titre écrit"
		  Return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub determineEnvironnement()
		  //Initialize and verify if on Production or Test
		  //We'll use an INI settings file to read the environment value
		  
		  #if DebugBuild then
		    UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  #else
		    UserSettings = GetFolderItem("ProdOuTest.ini")
		  #Endif
		  
		  Dim folderItem As FolderItem = UserSettings
		  OpenINI(UserSettings)
		  
		  environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  
		  host = HOSTPROD
		  If environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		    host = HOSTPREPRODUCTION
		  ElseIf environnementProp = "Test" Then  // Nous sommes en test
		    host = HOSTTEST
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function estimeReelExtr(projet_no_param As String) As Dictionary
		  Dim strSQL As String = ""
		  Dim estimeDict As New Dictionary 
		  
		  // Lecture de l'estimé pour le revenu total
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As EstimeMontantFacture " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + projet_no_param +"'" + _
		  "          AND SUBSTRING(estime_rev_mo_mat,1,2) <> 'AJ' " 
		  Dim estimeReelRS As RecordSet = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantFacture") = estimeReelRS.Field("EstimeMontantFacture").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour le revenu  des demandes de changement
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As EstimeMontantFactureExtra " + _
		  "        FROM estime " + _ 
		  "       WHERE estime_projet_no = '" + projet_no_param +"'" + _
		  "          AND estime_rev_mo_mat = 'AJREV' " 
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantFactureExtra") = estimeReelRS.Field("EstimeMontantFactureExtra").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour la main d'oeuvre
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + projet_no_param +"'"   + _
		  "          AND estime_rev_mo_mat    IN( 'MO', 'AJMO')" 
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantMO") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture de l'estimé pour le matériel
		  strSQL = "SELECT ROUND(SUM(estime_montant),0) As Montant, ROUND(SUM(estime_montant_hors_taxe),0) As MontantAvecProfit " + _
		  "        FROM estime " + _ 
		  "      WHERE estime_projet_no = '" + projet_no_param +"'"   + _
		  "          AND estime_rev_mo_mat    IN('MAT', 'AJMAT')" 
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("EstimeMontantMAT") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les revenus (Facturation)
		  strSQL = "SELECT ROUND(SUM(realiserev_montant),0) As Montant, ROUND(SUM(realiserev_paiement),0) As Revenu " + _
		  "        FROM realiserev " + _ 
		  "       WHERE realiserev_projet_no = '" + projet_no_param +"'" 
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalFacture") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de main d'oeuvre
		  strSQL = _
		  "SELECT SUM(Montant) As Montant  FROM" + _
		  "   (SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + projet_no_param +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) = '6' "  + _
		  "   UNION " + _
		  "   SELECT ROUND(SUM(realisemo_salbrut+realisemo_das+realisemo_cnesst),0) As Montant " + _
		  "        FROM realisemo " + _ 
		  "       WHERE realisemo_projet_no = '" + projet_no_param +"'" + _
		  "   )" + _
		  "  AS MOTable "
		  
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMO") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour les dépenses de matériel
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + projet_no_param +"'"   + _
		  "          AND SUBSTR(realisedep_gl,1,1) IN('5','8') " 
		  
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalMAT") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  // Lecture du réalisé pour le PerDiem
		  strSQL = _
		  "SELECT ROUND(SUM(realisedep_montant),0) As Montant " + _
		  "        FROM realisedep " + _ 
		  "       WHERE realisedep_projet_no  = '" + projet_no_param +"'"   + _
		  "          AND realisedep_gl = '8995' " 
		  
		  estimeReelRS = bdGOP.SQLSelect(strSQL)
		  If estimeReelRS = Nil Then Exit
		  estimeReelRS.MoveFirst
		  estimeDict.Value("ReelMontantTotalPerDiem") = estimeReelRS.Field("Montant").DoubleValue
		  estimeReelRS.Close
		  estimeReelRS = Nil
		  
		  Return estimeDict
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert(tempString)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ouvertureBD(compagnie_param As String) As String
		  Dim message As String = "Succes"
		  
		  // Ouverture base de données de la compagnie
		  bdGOP = New PostgreSQLDatabase
		  bdGOP.Host = host
		  bdGOP.UserName = GenControlModule.USER
		  bdGOP.Password = GenControlModule.PASSWORD
		  bdGOP.DatabaseName = compagnie_param
		  
		  If Not (bdGOP.Connect) Then
		    message = GMTextConversion(bdGOP.ErrorMessage)
		  Else
		    bdGOP.SQLExecute("SET search_path TO dashboard, equip, dbglobal, portail, locking, audit, public;")
		    If  bdGOP .Error Then
		      message = GMTextConversion(bdGOP.ErrorMessage)
		    End If
		  End If
		  
		  Return message
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		bdGOP As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty


#tag EndWindowCode

#tag Events MainWindowPushButton
	#tag Event
		Sub Action()
		  createChiffrierCompagnie(CompagniePopupMenu.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="environnementProp"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="host"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
